set previewer ~/.config/lf/preview
set preview True
set cleaner ~/.config/lf/cleaner
# Basic vars

set shell bash
set shellopts '-eu'
set ifs "\n"
set scrolloff 10
set icons
set period 1
set hiddenfiles ".*:*.aux:*.log:*.bbl:*.bcf:*.blg:*.run.xml"

# Vars that depend on environmental variables
#$lf -remote "send $id set previewer ~/.config/lf/scope"


# cmds/functions
cmd open ${{
    case $(file --mime-type "$(readlink -f $f)" -b) in
	application/epub+zip) setsid -f atril $fx >/dev/null 2>&1 ;;
	image/vnd.djvu|application/pdf|application/postscript) setsid -f zathura $fx >/dev/null 2>&1 ;;
        text/*) $EDITOR $fx;;
	image/x-xcf) setsid -f gimp $fx >/dev/null 2>&1 ;;
	image/svg+xml) display -- $fx ;;
	image/*) setsid -f sxiv-rifle $fx ;;
	# audio/flac) st mpv $fx --input-ipc-server=/tmp/mpvsocketm;; 
	application/octet-stream|audio/*) mpv --volume=80 $fx --input-ipc-server=/tmp/mpvsocketm;; 
	application/octet-stream) 
        img=$(readlink -f $f | rev | cut -d "." -f1 | rev)
        #notify-send $img
        case "$img" in
            qoi) sxiv-rifle $fx 
                exit 0
            ;;
            *) $fx --input-ipc-server=/tmp/mpvsocketm
            ;;
        esac
        mpv --volume=80 $fx --input-ipc-server=/tmp/mpvsocketm
        ;; 
	audio/*) mpv --volume=80 $fx --input-ipc-server=/tmp/mpvsocketm;; 
	# application/octet-stream|audio/*) deadbeef $fx;; 
# mpv --audio-display=no $fx 
	video/*) setsid -f mpv --volume=80 $fx -quiet --input-ipc-server=/tmp/mpvsocket >/dev/null 2>&1 ;;
	application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
	application/pgp-encrypted) $EDITOR $fx ;;
        *) for f in $fx; do setsid -f $OPENER $f >/dev/null 2>&1; done;;
    esac
}}


cmd extract ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	printf "%s\n\t" "$fx"
	printf "extract?[y/N]"
	read ans
	[ $ans = "y" ] && aunpack "$fx"
}}

cmd delete ${{
	#clear; tput cup $(($(tput lines)/3)); tput bold
	#set -f
	#printf "%s\n\t" "$fx"
	#printf "delete?[y/N/t]"
	#read ans
    ans=$(echo -e "t\ny\nn" | fzf)
	[ $ans = "y" ] && rm -rf -- $fx
	[ $ans = "t" ] && mv $fx ~/.trash
}}
cmd moveto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Move to where?"
	dest="$(cat ~/.local/bin/other/termfolders | fzf | sed 's|~|$HOME|')" &&
	for x in $fx; do
		eval mv -iv \"$x\" \"$dest\"
	done &&
	notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd copyto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Copy to where?"
	dest="cat ~/.local/bin/other/termfolders | fzf | sed 's|~|$HOME|')" &&
	for x in $fx; do
		eval cp -ivr \"$x\" \"$dest\"
	done &&
	notify-send "📋 File(s) copied." "File(s) copies to $dest."
}}

cmd mkdir ${{
    printf "Dir Name: "
    read ans
    mkdir $ans
}}

cmd dnd ${{
	#clear; tput cup $(($(tput lines)/3)); tput bold
	#set -f
	for x in $fx; do
	    eval dragon-drag-and-drop -a -x '$x'
	done 
}}

cmd pathcopy ${{
    end="" 
    for x in $fx; do
        #eval notify-send "hello" '$x'
        end=$end' '$x
    done
    #end=$(echo "$end" | grep -v '^$')
    #end="hello\n2"
    echo "$end" | xclip -se c && notify-send "Copied to clipboard" "$end"
}}

cmd thumb ${{
    rm -f /home/archbtw/.cache/lfthumb/*
    #handle non videos
    for x in $fx; do
        stem=$(echo $x | rev | cut -d'/' -f1 | rev)
        name='/home/archbtw/.cache/lfthumb/'$stem'.jpg'
        #eval notify-send $name $x
        eval mpv $x --loop-file=no --frames=1 -o=$name
    done 
    sxiv-rifle /home/archbtw/.cache/lfthumb/*
}}

cmd browser ${{
	for x in $fx; do
	    eval $BROWSER '$x'
	done 
}}

cmd nvim ${{
	for x in $fx; do
	    eval $EDITOR '$x'
	done 
}}

cmd grab ${{
    file=$(find ~ -maxdepth 2 -type f | grep -v "^\." | dmenu)
    [ -z $file ] && exit 1
    mv $file .
}}


cmd bulk-rename ${{
	index=$(mktemp /tmp/lf-bulk-rename-index.XXXXXXXXXX)
	if [ -n "${fs}" ]; then
		echo "$fs" > $index
	else
		echo "$(ls "$(dirname $f)" | tr ' ' "\ ")" > $index
	fi
	index_edit=$(mktemp /tmp/lf-bulk-rename.XXXXXXXXXX)
	cat $index > $index_edit
	$EDITOR $index_edit
	if [ $(cat $index | wc -l) -eq $(cat $index_edit | wc -l) ]; then
		max=$(($(cat $index | wc -l)+1))
		counter=1
		while [ $counter -le $max ]; do
			a="$(cat $index | sed "${counter}q;d")"
			b="$(cat $index_edit | sed "${counter}q;d")"
			counter=$(($counter+1))
			
			[ "$a" = "$b" ] && continue
			[ -e "$b" ] && echo "File exists: $b" && continue
			mv "$a" "$b"
		done
	else
		echo "Number of lines must stay the same"
	fi
	rm $index $index_edit
}}


cmd bg-set "latest" "$1"
cmd bulkrename $vidir

# Bindings
map ü pathcopy
map t thumb
map <c-f> $lf -remote "send $id select '$(fzf)'"
#map mp console shell mv %%s ~/data/pictures/
map J $lf -remote "send $id cd '$(cat ~/.local/bin/other/termfolders | fzf | tee /tmp/lastfolderlf)'"
map <c-j> $lf -remote "send $id cd '$(cat /tmp/lastfolderlf)'"
map gh
map gg top
map D delete
map E extract
map C copyto
map M moveto
map <c-n> push :mkdir<space>
map <c-r> reload
map <backspace2> set hidden!
map <enter> shell
map x $$f
map X !$f
#map o &mimeopen $f
map O $mimeopen --ask $f

map A rename # at the very end
map c push A<c-u> # new rename
map F push A<c-a> # at the very beginning
map f push A<a-b><a-b><a-f> # before extention
map a push A<a-b> # after extention
map B bulk-rename
map b $bg-set $f

map k down
map i up
map j updir 
map c copy
map v paste
map x cut
map V push :!nvim<space>

map W $setsid -f $TERMINAL >/dev/null 2>&1
map ä mkdir
map zf filter
map zF setfilter

# Source Bookmarks
source "~/.config/lf/bookmarks"

map o dnd 
map ö browser

#map mkd console shell mkdir
#map sv console shell sudo vim %s #to fix
#map nv console shell nvim %s #to fix
#map br bulkrename %%s
#map py console shell python %s

#map sx console shell sxiv *.{png,jpg,gif}
#map st console shell nohup st &

